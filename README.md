  - Create Secret in Kubernets Cluster:

    - Note: Secret must be in same namepsace where you are creating the deployment

    - Option 1: From command line 

      - kubectl create secret generic my-registry-key \
        --from-file=.dockerconfigjson=.docker/config.json
        --type=kubernetes.io/dockerconfigjson

    - Option 2: From YAML file 

      apiVersion: v1
      kind: Secret
      metadata:
        name: my-registry-key
      data:
        .dockerconfigjson: <paste the base64 value of .docker/config.json file>
      type: kubernetes.io/dockerconfigjson


    - Option 3: Using docker-server and username/password 
      
      - kubectl create secret generic my-registry-key-two \ 
        --docker-server=https://780743570903.dkr.ecr.us-east-1.amazonaws.com \
        --docker-username=AWS \
        --docker-password=<password_key_from_aws_ecr_get_login_command>"
